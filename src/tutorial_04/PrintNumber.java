package tutorial_04;

/**
 * Created by grainier on 2/20/15.
 */
public class PrintNumber extends Thread {
    public PrintNumber(String name){
        super(name);
    }

    public void run() {
        for (int i = 0;; i++) {
            System.out.println(getName() + " : " + i);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
