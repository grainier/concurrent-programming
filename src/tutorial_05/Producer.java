package tutorial_05;

/**
 * Created by grainier on 2/27/15.
 */
public class Producer extends Thread {
    private SimpleInbox inbox;

    public Producer(SimpleInbox inbox) {
        super("Producer");
        this.inbox = inbox;
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                this.inbox.produce(i);
                System.out.println("Produced: " + i);
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
