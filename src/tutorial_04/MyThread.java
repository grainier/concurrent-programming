package tutorial_04;

import java.util.Scanner;

/**
 * Created by grainier on 2/20/15.
 */
public class MyThread {
    public static void main(String[] args) {
        System.out.println("Input Something");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Input was : " + str);
    }
}
