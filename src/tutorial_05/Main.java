package tutorial_05;

/**
 * Created by grainier on 2/26/15.
 */
public class Main {
    public static void main(String[] args) {
        producerConsumerProblem();
    }

    public static void producerConsumerProblem() {
        SimpleInbox inbox = SimpleInbox.getInstance();
        Producer producer = new Producer(inbox);
        Consumer consumer = new Consumer(inbox);

        producer.start();
        consumer.start();
    }
}
