package tutorial_04;

/**
 * Created by grainier on 2/20/15.
 */
public class BankAccount {

    private double balance = 0;

    public synchronized double getBalance() {
        return balance;
    }

    public synchronized void withDraw(double amt) {
        this.balance -= amt;
        System.out.println("new balance : " + this.balance);
    }

    public synchronized void deposit(double amt) {
        this.balance += amt;
        System.out.println("new balance : " + this.balance);
    }

}
