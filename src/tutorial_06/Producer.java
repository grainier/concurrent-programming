package tutorial_06;

/**
 * Created by grainier on 2/27/15.
 */
public class Producer extends Thread {
    private SimpleInbox inbox;

    public Producer(String name, SimpleInbox inbox) {
        super(name);
        this.inbox = inbox;
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                System.out.println(getName() + " is Producing : " + i);
                this.inbox.produce(i);
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
