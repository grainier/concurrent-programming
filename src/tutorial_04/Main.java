package tutorial_04;

import java.util.Scanner;

/**
 * Created by grainier on 2/20/15.
 */
public class Main {
    public static void main(String[] args) {

        bankTutorial();

        // daemonMultiThreadTutorial();

    }

    public static void bankTutorial() {
        BankAccount bankAccount = new BankAccount();
        Granny granny = new Granny(bankAccount);
        Student student = new Student(bankAccount);
        BankManager bankManager = new BankManager(bankAccount);
        LoanCompany loanCompany = new LoanCompany(bankAccount);

        granny.start();
        student.start();
        bankManager.start();
        loanCompany.start();
    }

    public static void daemonMultiThreadTutorial() {
        Thread t1 = new PrintNumber("t1");
        t1.setDaemon(true);
        t1.start();

        Thread t2 = new PrintNumber("t2");
        t2.setDaemon(true);
        t2.start();

        Thread t3 = new PrintNumber("t3");
        t3.setDaemon(true);
        t3.start();

        runnableSample();

        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runnableSample() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                for(int x = 0;; x++) {
                    System.out.println(Thread.currentThread().getName() + " : " + x);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "R1");
        t.start();
    }
}
