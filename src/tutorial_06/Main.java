package tutorial_06;

/**
 * Created by grainier on 2/26/15.
 */
public class Main {
    public static void main(String[] args) {
        producerConsumerProblem();
    }

    public static void producerConsumerProblem() {
        SimpleInbox inbox = SimpleInbox.getInstance();
        Producer producer_1 = new Producer("Producer 1", inbox);
        Producer producer_2 = new Producer("Producer 2", inbox);
        Consumer consumer_1 = new Consumer("Consumer 1", inbox);
        Consumer consumer_2 = new Consumer("Consumer 2", inbox);
        Consumer consumer_3 = new Consumer("Consumer 3", inbox);

        producer_1.start();
        producer_2.start();
        consumer_1.start();
        consumer_2.start();
        consumer_3.start();
    }
}
