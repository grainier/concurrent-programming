package tutorial_06;

/**
 * Created by grainier on 2/27/15.
 */
public class Consumer extends Thread {
    private SimpleInbox inbox;

    public Consumer(String name, SimpleInbox inbox) {
        super(name);
        this.inbox = inbox;
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                System.out.println(getName() + " consumed : " + this.inbox.consume());
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
