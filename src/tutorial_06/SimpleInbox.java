package tutorial_06;

import java.util.Vector;
import java.util.concurrent.Semaphore;

/**
 * Created by grainier on 2/20/15.
 */
public class SimpleInbox {
    private final Vector queue = new Vector();
    private int size = 6;
    private static SimpleInbox instance;

    // semaphore for array
    private final Semaphore freeSlots = new Semaphore(size, true);
    private final Semaphore availableSlots = new Semaphore(0, true);

    // semaphore for produce and consume
    private final Semaphore accessing = new Semaphore(1, true);

    private SimpleInbox() {
    }

    public static SimpleInbox getInstance() {
        if (instance == null) {
            synchronized (SimpleInbox.class) {
                if (instance == null) {
                    instance = new SimpleInbox();
                }
            }
        }
        return instance;
    }

    public void produce(int i) throws InterruptedException {
        // if queue is full
        if (this.freeSlots.availablePermits() == 0) {
            System.out.println("Queue is full " + Thread.currentThread().getName() + " is waiting , size: " + this.queue.size());
        }

        // Otherwise acquire slot and produce
        this.freeSlots.acquire();
        this.accessing.acquire();
        this.queue.add(i);
        System.out.println("Produced : " + i + " In Slot : " + this.queue.size());
        this.accessing.release();
        this.availableSlots.release();
    }

    public int consume() throws InterruptedException {
        // if queue is empty
        if (this.availableSlots.availablePermits() == 0) {
            System.out.println("Queue is empty " + Thread.currentThread().getName() + " is waiting , size: " + this.queue.size());
        }

        // Otherwise acquire slot and consume
        this.availableSlots.acquire();
        this.accessing.acquire();
        int val =  (Integer) this.queue.remove(0);
        this.accessing.release();
        this.freeSlots.release();
        return val;
    }
}
