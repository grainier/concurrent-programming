package tutorial_04;

/**
 * Created by grainier on 2/20/15.
 */
public class LoanCompany extends Thread {
    private BankAccount account;

    public LoanCompany(BankAccount bankAccount) {
        super("LoanCompany");
        this.account = bankAccount;
    }

    public void run() {
        account.deposit(500);
    }
}
