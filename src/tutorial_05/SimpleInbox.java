package tutorial_05;

import java.util.Vector;
import java.util.concurrent.Semaphore;

/**
 * Created by grainier on 2/20/15.
 */
public class SimpleInbox {
    // private int inbox = 0;
    // private volatile boolean flag = false;
    private final Vector queue = new Vector();
    private int size = 5;
    private static SimpleInbox instance;

    private SimpleInbox() {
    }

    public static SimpleInbox getInstance() {
        if (instance == null) {
            synchronized (SimpleInbox.class) {
                if (instance == null) {
                    instance = new SimpleInbox();
                }
            }
        }
        return instance;
    }

    /*
    public synchronized void produce(int i) {
        while (this.flag) {
            try {
                System.out.println(Thread.currentThread().getName() + " : Not consumed value exists, waiting...");
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.inbox = i;
        this.flag = true;
        System.out.println(Thread.currentThread().getName() + " : Done waiting, produced new value.");
        notifyAll();
    }
    */

    public void produce(int i) throws InterruptedException {

        // wait if queue is full
        while (this.queue.size() == this.size) {
            synchronized (this.queue) {
                System.out.println("Queue is full " + Thread.currentThread().getName() + " is waiting , size: " + this.queue.size());
                this.queue.wait();
            }
        }

        // producing element and notify consumers
        synchronized (this.queue) {
            this.queue.add(i);
            this.queue.notifyAll();
        }
    }

    /*
    public synchronized int consume() {
        while (!this.flag) {
            try {
                System.out.println(Thread.currentThread().getName() + " : No value been produced, waiting...");
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.flag = false;
        System.out.println(Thread.currentThread().getName() + " : Done waiting, consumed new value.");
        notifyAll();
        return this.inbox;
    }
    */

    public int consume() throws InterruptedException {
        //wait if queue is empty
        while (this.queue.isEmpty()) {
            synchronized (this.queue) {
                System.out.println("Queue is empty " + Thread.currentThread().getName() + " is waiting , size: " + this.queue.size());
                this.queue.wait();
            }
        }

        //Otherwise consume element and notify waiting producer
        synchronized (this.queue) {
            this.queue.notifyAll();
            return (Integer) this.queue.remove(0);
        }
    }

}
