package tutorial_04;

/**
 * Created by grainier on 2/20/15.
 */
public class Granny extends Thread {
    private BankAccount account;

    public Granny(BankAccount bankAccount) {
        super("Granny");
        this.account = bankAccount;
    }

    public void run() {
        account.deposit(200);
    }
}
