package tutorial_04;

/**
 * Created by grainier on 2/20/15.
 */
public class Student extends Thread {
    private BankAccount account;

    public Student(BankAccount bankAccount) {
        super("Student");
        this.account = bankAccount;
    }

    public void run() {
        account.withDraw(600);
    }
}
