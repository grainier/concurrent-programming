package tutorial_04;

/**
 * Created by grainier on 2/20/15.
 */
public class BankManager extends Thread {
    private BankAccount account;

    public BankManager(BankAccount bankAccount) {
        super("BankManager");
        this.account = bankAccount;
    }

    public void run() {
        for (int i = 0;; i++) {
            try {
                if (account.getBalance() < 0) {
                    account.withDraw(50);
                }
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
