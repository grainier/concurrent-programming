/**
 * Created by grainier on 2/27/15.
 */

import java.applet.Applet;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main extends Applet implements Runnable {
    Thread t, t1;

    public void start() {
        t = new Thread(this);
        t.start();
    }

    public void run() {
        t1 = Thread.currentThread();
        while (t1 == t) {
            repaint();
            try {
                t1.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void stop() {
        t = null;
    }

    public void paint(Graphics g) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
        Calendar cal = new GregorianCalendar();
        g.drawString(sdf.format(cal.getTime()), 50, 30);
    }
}